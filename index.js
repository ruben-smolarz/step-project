const serviceItems = $('.service-list li')
const serviceList = $('.service-list')
const servicePhoto = $('.service-info-block-photo')
const serviceParagraph = $('.service-info-block p')
const loremArray = [
	'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
	'Nulla sed justo vel odio ullamcorper posuere a et tellus.',
	'Donec sit amet felis eu sapien interdum mattis.',
	'Nam sed massa nec ligula fringilla faucibus vel a nisi.',
	'Morbi maximus tellus eu turpis finibus, quis malesuada arcu feugiat.',
	'Sed in bibendum velit, eu elementum justo.',
	'Quisque vel turpis sit amet massa rutrum bibendum.',
	'Praesent in nunc enim.',
	'Suspendisse id ultrices leo.',
	'Fusce volutpat, tortor eu mattis tristique, nisl velit bibendum sapien, ac eleifend tellus augue a ante.',
	'Aliquam bibendum malesuada nisi non tempor.',
	'Suspendisse potenti.',
	'Etiam ut velit quam.',
	'Cras ac libero vel enim fringilla mollis.',
	'Phasellus et consequat augue, ac rutrum leo.',
	'Mauris pharetra nisl id nulla vulputate congue.',
	'Proin nec ex quis libero scelerisque interdum.',
	'Aenean ac posuere ex.',
	'In sit amet magna volutpat, malesuada dolor ut, malesuada ex.',
]
const logoContainer = $('.logo-container')
const logoLetter = $('.logo span')
const searchIcon = $('.svg-search')
const searchInput = $('.input-box input')
const serviceInfoBlock = $('.service-info-block')
randomText = () => {
	let loremText = ''
	for (let i = 0; i < 16; i++) {
		const randomIndex = Math.floor(Math.random() * loremArray.length)
		loremText += loremArray[randomIndex] + ' '
	}
	$(serviceParagraph).html(`<p>${loremText}</p>`)
}
indexPhoto = (index = 0) => {
	$(servicePhoto).css({
		height: $(serviceParagraph).height() + 'px',
		backgroundImage: `url(./image/main-scd-block/service-${index}.jpeg)`,
	})
}
randomText()
indexPhoto()
$(serviceItems).click(function () {
	$(this).siblings().removeClass('active-service')
	$(this).addClass('active-service')
	$(this).css({
		transition: 'transform 0.3s ease-in-out',
		transform: 'translateY(-25px)',
	})
	$(serviceInfoBlock).css({
		transition: 'all 0.3s ease-in-out',
		transform: 'translateY(30px)',
		opacity: 0,
	})
	setTimeout(() => {
		randomText()
		indexPhoto($(this).index())
	}, 300)

	setTimeout(() => {
		$(this).css({
			transform: 'translateY(0px)',
		})
	}, 330)
	setTimeout(() => {
		$(serviceInfoBlock).css({
			transform: 'translateY(0px)',
			opacity: 1,
		})
	}, 380)
})

$(logoContainer).mouseenter(function () {
	$(logoLetter).css({
		transition: 'transform 0.3s ease-in-out',
		transform: 'rotateZ(180deg)',
	})
})
$(logoContainer).mouseleave(function () {
	$(logoLetter).css({
		transition: 'transform 0.3s ease-in-out',
		transform: 'rotateZ(-180deg)',
	})
})
let searchIconClick = false
let searchIconDeg = null

searchIcon.click(function () {
	if (searchIconClick === false) {
		searchIconDeg = '90deg'
		searchIconClick = true
	} else {
		searchIconDeg = '0deg'
		searchIconClick = false
	}
	$(this).css({
		transition: 'transform 0.3s ease-in-out',
		transform: `rotateZ(${searchIconDeg})`,
	})
	$(searchInput).css({
		opacity: `${searchIconClick ? 1 : 0}`,
		pointerEvents: `${searchIconClick ? 'auto' : 'none'}`,
	})
})
